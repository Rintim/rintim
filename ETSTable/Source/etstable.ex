defmodule Etstable do
  @moduledoc """
  Documentation for `Etstable`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Etstable.hello()
      :world

  """
  def hello do
    :world
  end

	def start_link(state \\ %{})

	def start_link(state) when is_map(state) do
		GenServer.start_link(__MODULE__.Server, state, name: __MODULE__.Server)
	end

	def store(key, value), do: GenServer.cast(__MODULE__.Server, {:store, {key, value}})

	def get(key), do: GenServer.call(__MODULE__.Server, {:get, key})

	def get_key, do: GenServer.call(__MODULE__.Server, :get_key)

	def has_key?(key), do: GenServer.call(__MODULE__.Server, {:has_key, key})

	def stop() do
		GenServer.stop(__MODULE__.Server)
	end
end
