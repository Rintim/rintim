
defmodule Etstable.Server do
  @moduledoc """
  Documentation for `Etstable`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Etstable.hello()
      :world

  """

	use GenServer

	def handle_cast({:store, item}, env) do
		:ets.insert(env, item)
		{:noreply,  env}
	end

	def handle_call({:get, key}, _from, env) do
		pattern = [{{key, :"$1"}, [], [:"$1"]}]
		item = :ets.select(env, pattern)
		result =
			case item do
				[item] ->
					item
				[] ->
					nil
				_ ->
					:error
			end

		{:reply, result, env}
	end

	def handle_call(:get_key, _from, env) do
		pattern = [{{:"$1", :_}, [], [:"$1"]}]
		result = :ets.select(env, pattern)

		{:reply, result, env}
	end

	def handle_call({:has_key, key}, _from, env) do
		pattern = [{{key, :_}, [], [true]}]
		result =
			:ets.select(env, pattern)
			|> Enum.empty?
			|> Kernel.==(false)
		{:reply, result, env}
	end

	def init(state) when is_map(state) do
		table = :ets.new(:etstable, [:set, :protected])
		Enum.map(state, fn {key, value} ->
			:ets.insert(table, {key, value})
		end)
		if !Map.has_key?(state, "uuid"), do: :ets.insert(table, {:uuid, UUID.uuid4()})
		{:ok, table}
	end
end
