defmodule AlgorithmTest do
  use ExUnit.Case
  doctest Algorithm

  test "greets the world" do
    assert Algorithm.hello() == :world
  end

	test "Comp" do
		assert Algorithm.comp(4, 3) == [[1, 2, 3], [1, 2, 4], [1, 3, 4], [2, 3, 4]]
		assert Algorithm.comp([1, 3, 5, 7], 3) == [[1, 3, 5], [1, 3, 7], [1, 5, 7], [3, 5, 7]]
	end
end
