defmodule Algorithm do
  @moduledoc """
  Documentation for `Algorithm`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Algorithm.hello()
      :world

  """
  def hello do
    :world
  end

  @doc """
  In mathematics, a combination is a selection of items from a collection, such that the order of selection does not matter (unlike permutations). For example, given three fruits, say an apple, an orange and a pear, there are three combinations of two that can be drawn from this set: an apple and a pear; an apple and an orange; or a pear and an orange. More formally, a k-combination of a set S is a subset of k distinct elements of S. If the set has n elements, the number of k-combinations is equal to the binomial coefficient.

  ## Parameters

    - k: This argument can be two type: list and integer.
      - list: directly-passed list
      - integer: as "k" in maths.
    - n: as "n" in maths. REMEMBER, n shoule less than k or length or k

  ## Examples

      iex> Algorithm.comp(4, 3)
      [[1, 2, 3], [1, 2, 4], [1, 3, 4], [2, 3, 4]]

      iex> Algorithm.comp([1, 3, 5, 7], 3)
      [[1, 3, 5], [1, 3, 7], [1, 5, 7], [3, 5, 7]]

  """
  @spec comp(list | integer, integer) :: [list]
  def comp(k, n)

  def comp(list, n) when is_list(list) and is_integer(n) and n > 0 and length(list) >= n,
    do: comp(list, n, [], [], [])

  def comp(k, n) when is_integer(k) and is_integer(n) and k > 0 and n > 0 and n <= k,
    do:
      1..k//1
      |> Enum.to_list()
      |> comp(n, [], [], [])

  defp comp(_, 0, [previous | rest], tmp_result = [_ | prer], result),
    do: comp(previous, 1, rest, prer, [Enum.reverse(tmp_result) | result])

  defp comp([], n, [previous | rest], [_ | prer], result),
    do: comp(previous, n + 1, rest, prer, result)

  defp comp([], _, _, _, result), do: Enum.reverse(result)

  defp comp([head | tail], n, stack, tmp_result, result),
    do: comp(tail, n - 1, [tail | stack], [head | tmp_result], result)
    
end
